# adacraft dashboard

This project is part of the [adacraft](https://www.adacraft.org/), and
implements a dashboard to follow recent activities in adacraft portal.

This is a static website developped in Vue 2 and Tailwind CSS. It connects to a
Fauna database to get recent updates.

For more information about adacraft and its developement, go to the [main
project page](https://gitlab.com/adacraft/main).

## Instructions

We use `npm` as our package manager.

To initilaize your working directory:

```
$ npm install
```

To launch the dashboard during development:

```
$ npm run serve
```

To build the dashboard for deployement:

```
$ npm run build
```